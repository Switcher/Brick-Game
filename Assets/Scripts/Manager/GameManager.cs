﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    float slowness = 8f;

    public static bool end;
    public static bool soundOn = true;

    public int score;
    public static int highScore;
    
    public Text textGameOver,textScore,textHighScore;

    public Toggle mToggle;

    AudioSource[] audioSource;
    public AudioClip addScoreClip, gameOverClip;

    void Start ()
    {
        end = false;
    }

    void Update()
    {
        if (!soundOn)
        {
            foreach (AudioSource audio in audioSource)
            {
                audio.mute = true;
            }
        }
        else
        {
            foreach (AudioSource audio in audioSource)
            {
                audio.mute = false;
            }
        }
    }
 

    void Awake()
    {

        audioSource = GetComponents<AudioSource>();
      

    }

    public void SetSFX()
    {
       
        if (soundOn)
            soundOn = false;
        else
            soundOn = true;

    }

    public void AddScore()
    {
    
            audioSource[0].clip = addScoreClip;
            audioSource[0].Play();
            score++;
            textScore.text = "Score: " + score;

    }

    public void EndGame()
    {
        StartCoroutine(Restart());
        end = true;
    }

    public void SetHighScore()
    {
        if(score > highScore)
        {
            highScore = score;
        }

    }

    IEnumerator Restart()
    {
        audioSource[0].clip = gameOverClip;
        audioSource[0].Play();
        Time.timeScale = 1f / slowness;
        Time.fixedDeltaTime = Time.fixedDeltaTime / slowness;
        textGameOver.text = "Game Over !";
        SetHighScore();
        textHighScore.text = "High score: " + highScore;
        Application.ExternalCall("kongregate.stats.submit", "HighScore", score);

        yield return new WaitForSeconds(gameOverClip.length / slowness);

        textHighScore.text = " ";
        textGameOver.text = "";
        textScore.text = "Score: 0";

        Time.fixedDeltaTime = Time.fixedDeltaTime * slowness;
        Time.timeScale = 1f;
        SceneManager.LoadScene(Random.Range(0, 3));
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);


    }

}
