﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckToggle : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        var toggle = GetComponent<UnityEngine.UI.Toggle>();

        if (GameManager.soundOn == false)
        {
            toggle.isOn = false;
            GameManager.soundOn = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
