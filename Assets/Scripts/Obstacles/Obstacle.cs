﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {



	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody2D>().gravityScale += Time.timeSinceLevelLoad / 20f;
    }
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y <= -6f)
            Destroy(gameObject);
	}
}
