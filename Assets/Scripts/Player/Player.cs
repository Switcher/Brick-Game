﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {


    public GameManager gameManager = new GameManager();
    public float speed = 15f;
    float width = 4.5f;

    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float horiz = Input.GetAxis("Horizontal") * Time.fixedDeltaTime;

        Vector2 newPosition = rb.position + Vector2.right * horiz * speed;

        newPosition.x = Mathf.Clamp(newPosition.x, -width, width);
        rb.MovePosition(newPosition);
	}

    void OnCollisionEnter2D()
    {
        if (!GameManager.end)
            gameManager.EndGame();

    }

    void OnTriggerEnter2D()
    {
        if(!GameManager.end)
        gameManager.AddScore();

    }
}
